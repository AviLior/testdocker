import click

@click.command()
@click.option('--count', default=1, help="Number of greetings")
def main(count):
    for x in range(count):
        print("Hello there")

if __name__ == '__main__':
    main()
